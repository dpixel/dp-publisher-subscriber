package changemanager

import (
	"gitlab.com/dpixel/dp-publisher-subscriber/models/errors"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/event"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/state"
	repochangemanager "gitlab.com/dpixel/dp-publisher-subscriber/repositories/changemanager"
)

type changeManager struct {
}

func New() (repochangemanager.ChangeManager[event.Event, state.State], error) {
	return &changeManager{}, nil
}

func (changeManager changeManager) Convert(event *event.Event) (*state.State, error) {
	if event == nil {
		return nil, errors.ErrInvalidEvent
	}

	return &state.State{
		Message: event.Message,
	}, nil
}
