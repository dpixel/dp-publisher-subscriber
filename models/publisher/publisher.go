package publisher

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/dpixel/dp-publisher-subscriber/models/errors"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/event"
	repopublisher "gitlab.com/dpixel/dp-publisher-subscriber/repositories/publisher"
	reposubscriber "gitlab.com/dpixel/dp-publisher-subscriber/repositories/subscriber"
)

type publisher struct {
	chanPublisher chan event.Event
	chanAttacher  chan reposubscriber.Subscriber[event.Event]
	chanDetacher  chan reposubscriber.Subscriber[event.Event]
	chanStop      chan bool
	subscribers   []reposubscriber.Subscriber[event.Event]
}

func New() (repopublisher.Publisher[event.Event, reposubscriber.Subscriber[event.Event]], error) {
	return &publisher{
		chanPublisher: make(chan event.Event),
		chanStop:      make(chan bool),
		chanAttacher:  make(chan reposubscriber.Subscriber[event.Event]),
		chanDetacher:  make(chan reposubscriber.Subscriber[event.Event]),
	}, nil
}

func (publisher *publisher) Attach(newSubscriber *reposubscriber.Subscriber[event.Event]) error {
	if newSubscriber == nil {
		return errors.ErrInvalidSubscriber
	}

	for _, subscriber := range publisher.subscribers {
		if subscriber == *newSubscriber {
			return errors.ErrSubscriberAlreadyExists
		}
	}

	publisher.chanAttacher <- *newSubscriber

	return nil
}

func (publisher *publisher) Detach(newSubscriber *reposubscriber.Subscriber[event.Event]) error {
	if newSubscriber == nil {
		return errors.ErrInvalidSubscriber
	}

	publisher.chanDetacher <- *newSubscriber

	return nil
}

func (publisher *publisher) Publish(newEvent *event.Event) error {
	if newEvent == nil {
		return errors.ErrInvalidEvent
	}

	publisher.chanPublisher <- *newEvent

	return nil
}

func (publisher publisher) Stop() {
	signalChanel := make(chan os.Signal, 1)
	signal.Notify(signalChanel,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	exit_chan := make(chan int)
	go func() {
		for {
			s := <-signalChanel
			switch s {
			case syscall.SIGHUP:
				fmt.Println("Signal hang up triggered.")
				close(publisher.chanStop)
				exit_chan <- 0
			case syscall.SIGINT:
				fmt.Println("Signal interrupt triggered.")
				close(publisher.chanStop)
				exit_chan <- 0
			case syscall.SIGTERM:
				fmt.Println("Signal terminate triggered.")
				close(publisher.chanStop)
				exit_chan <- 0
			case syscall.SIGQUIT:
				fmt.Println("Signal quit triggered.")
				close(publisher.chanStop)
				exit_chan <- 0
			default:
				fmt.Println("Unknown signal.")
				close(publisher.chanStop)
				exit_chan <- 1
			}
		}
	}()
	exitCode := <-exit_chan
	os.Exit(exitCode)
}

func (publisher *publisher) Start() {
	for {
		select {
		case event := <-publisher.chanPublisher:
			for _, subscriber := range publisher.subscribers {
				subscriber.Update(&event)
				subscriber.GetState()
			}
		case subscriber := <-publisher.chanAttacher:
			publisher.subscribers = append(publisher.subscribers, subscriber)
		case newSubscriber := <-publisher.chanDetacher:
			for i, subscriber := range publisher.subscribers {
				if subscriber == newSubscriber {
					publisher.subscribers = append(publisher.subscribers[:i], publisher.subscribers[i+1:]...)
					continue
				}
			}
		case <-publisher.chanStop:
			close(publisher.chanPublisher)
			close(publisher.chanAttacher)
			return
		}
	}
}
