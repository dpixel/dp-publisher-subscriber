package subscriber

import (
	"fmt"

	"gitlab.com/dpixel/dp-publisher-subscriber/models/errors"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/event"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/state"
	"gitlab.com/dpixel/dp-publisher-subscriber/repositories/changemanager"
	reposubscriber "gitlab.com/dpixel/dp-publisher-subscriber/repositories/subscriber"
)

type object struct {
	state state.State
}

type subscriber struct {
	object        object
	name          string
	changemanager changemanager.ChangeManager[event.Event, state.State]
}

func New(name string, changemanager *changemanager.ChangeManager[event.Event, state.State]) (reposubscriber.Subscriber[event.Event], error) {
	if changemanager == nil {
		return nil, errors.ErrInvalidChangeManager
	}

	return &subscriber{
		name:          name,
		changemanager: *changemanager,
	}, nil
}

func (subscriber *subscriber) Update(event *event.Event) error {
	if event == nil {
		return errors.ErrInvalidEvent
	}

	state, err := subscriber.changemanager.Convert(event)
	if err != nil {
		return err
	}
	subscriber.object.state = *state

	return nil
}

func (subscriber subscriber) GetState() {
	fmt.Printf("Observer `%s`, Object State: %s\n", subscriber.name, subscriber.object.state.Message)
}
