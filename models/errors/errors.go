package errors

import "errors"

var (
	ErrInvalidSubscriber       = errors.New("invalid subscriber")
	ErrSubscriberAlreadyExists = errors.New("subscriber already attached")
	ErrSubscriberNotFound      = errors.New("subscriber not found")
	ErrInvalidChangeManager    = errors.New("invalid change manager")
	ErrInvalidEvent            = errors.New("invalid event")
)
