package publisher

type Publisher[Event any, Subscriber any] interface {
	Attach(subscriber *Subscriber) error
	Detach(subscriber *Subscriber) error
	Publish(event *Event) error
	Start()
	Stop()
}
