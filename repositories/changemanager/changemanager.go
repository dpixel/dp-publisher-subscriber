package changemanager

type ChangeManager[Event any, State any] interface {
	Convert(event *Event) (*State, error)
}
