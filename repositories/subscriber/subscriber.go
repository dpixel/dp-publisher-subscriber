package subscriber

type Subscriber[Event any] interface {
	Update(event *Event) error
	GetState()
}
