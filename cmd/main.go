package main

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/changemanager"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/event"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/publisher"
	"gitlab.com/dpixel/dp-publisher-subscriber/models/subscriber"
)

func main() {
	changemanager, _ := changemanager.New()
	subscriber_1, _ := subscriber.New("Subscriber 1", &changemanager)
	subscriber_2, _ := subscriber.New("Subscriber 2", &changemanager)
	subscriber_3, _ := subscriber.New("Subscriber 3", &changemanager)

	publisher, _ := publisher.New()
	go publisher.Start()

	publisher.Attach(&subscriber_1)
	publisher.Attach(&subscriber_2)
	publisher.Attach(&subscriber_3)

	for i := 0; i < 5; i++ {
		publisher.Publish(&event.Event{
			Id:      uuid.New(),
			Message: fmt.Sprintf("Update %d", i),
		})
	}

	fmt.Println("After detaching...")
	publisher.Detach(&subscriber_2)

	for i := 0; i < 3; i++ {
		publisher.Publish(&event.Event{
			Id:      uuid.New(),
			Message: fmt.Sprintf("Update %d", i),
		})
	}

	publisher.Detach(&subscriber_2)

	publisher.Stop()
}
